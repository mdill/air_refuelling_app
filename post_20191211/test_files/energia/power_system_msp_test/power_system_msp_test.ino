/*
 * Michael Dill - 2019
 * 
 * This is a simple sketch to test the power supply of the DART wireless unit.
 *
 * This sketch will set a pre-determined load (430 Ohms) to high, in place of
 * the pressure transducer and ZigBee transmitter, for 30 seconds.  After 30s of
 * "runtime," the MSP board will turn the load off and go to sleep for 15
 * minutes.  This should adequately put an "average" load on the power supply
 * during an 8-hour solar-powered test.
 *
 * With this simulated load, DART should be able to adequately determine the
 * power capabilities of both the solar supply device and the MSP load.
 *
 */

#define load    6       // Pin P1.4

void setup(){
    pinMode( load, OUTPUT );
    digitalWrite( load, LOW );
}

void loop(){
    digitalWrite( load, HIGH );     // Put a load on our system
    delay( 30000 );                 // Stay on for 30 seconds

    digitalWrite( load, LOW );      // Turn "off" our load
    //sleepSeconds( 900 );            // Sleep for 15 minutes
    sleepSeconds( 2 );
}
