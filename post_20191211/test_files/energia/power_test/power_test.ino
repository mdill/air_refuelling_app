#define   LOAD    10            // P2.2
#define   ZB      11            // P2.3

const byte sleepTime = 30;      // In seconds
const byte onTime = 5;          // In seconds

void setup(){
    pinMode( LOAD, OUTPUT );
    digitalWrite( LOAD, LOW );
    
    pinMode( ZB, OUTPUT );
    digitalWrite( ZB, LOW );
}

void sleep(){
    digitalWrite( ZB,   LOW );
    digitalWrite( LOAD, LOW );

    sleepSeconds( sleepTime );  // Sleep for 30 seconds
}

void wake(){
    digitalWrite( ZB,   HIGH );
    digitalWrite( LOAD, HIGH );

    delay( onTime * 1000 );     // Stay on for 5 seconds
}

void loop(){
    wake();
    sleep();
}
