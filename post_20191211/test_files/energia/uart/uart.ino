char ourWord[] = { 'o', 'n', 'o', 'm', 'a', 't', 'o', 'p', 'o', 'e', 'i', 'a' };
int stallTime = 250;

void setup() {
  // put your setup code here, to run once:
  Serial.begin( 9600 );
}

void loop() {
  // put your main code here, to run repeatedly: 
  for( int i = 0; i < 12; i++ ){
    Serial.print( ourWord[i] );
    delay( stallTime );
  }

  Serial.print( '\n' );
}

