String ourWord[] = { "First string", "2nd string", "This is the third",
                     "fourth stringer", "FIVER", "onomatopoeia", "STUFF" };


void setup(){
    Serial.begin( 9600 );
}

void loop(){
    for( int i = 0; i < sizeof( ourWord ) / sizeof( ourWord[0] ); i++ ){
        Serial.println( ourWord[i] );       // Print our strings
        delay( 500 );                       // 4Hz refresh rate
    }
}
