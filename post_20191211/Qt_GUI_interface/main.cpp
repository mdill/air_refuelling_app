#include "air_refuelling_app.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    air_refuelling_app w;
    w.show();

    return a.exec();
}
