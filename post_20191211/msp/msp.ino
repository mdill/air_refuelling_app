/*
 * Michael Dill - 2019
 *
 * This code is written for ECE4806 and Collins Aerospace to facilitate wireless
 * communication between an in-wing "pressure transmitter" and a ZigBee wireless
 * transmitter.
 *
 * The MCU will periodically sleep, only waking to "check" for a wake command,
 * in order to conserve as much power as possible.
 *
 * Upon receiving a wake command, the MCU will "hand-off" serial data from the
 * PT to the ZB transmitter, and check for a sleep command.  If no sleep command
 * is received, the program will run for 5 minutes, then go back to sleep -- if
 * a sleep command is received before that 5-minute expiration, the MCU will
 * immediately go back to sleep to conserve power.
 *
 */

#include <SPI.h>        // For ZigBee communications

// Frame byte values
const byte  startFrame  = 0x7E;
byte        messSize    = 0x00;
const byte  frameType   = 0x00;
const byte  frameID     = 0x01;
const byte  recvMAC[]   = { 0x00, 0x13, 0xA2, 0x00, 0x41, 0x94, 0xC9, 0xD6 };
const byte  options     = 0x00;
byte        checkSum    = 0x00;

int frameArrayLength;                   // Track how many bytes to send
byte frameArray[100];                   // Allocate enough memory for all bytes

// Allow XBee board to receive data for MSP board
#define     ZB_ss       8       // P2.0
#define     ZB_attn     13      // P2.5
#define     ZB_on       18      // P2.7

// Define sleep/wake commands from GUI
#define     wakeComm    0x57    // "W"
#define     sleepComm   0x53    // "S"

// Communicate with our "black box" through UART
#define     BAUDrate    9600

// Define a pin to "simulate" our PT load resistance
#define     PT_load     10      // P2.2

// Get our battery status with an analog pin
#define     battPin     5       // P1.3

// Determine if the MCU is in sleep/wake mode
bool wakeUp = false;                // Start in the "sleep" mode
// Set the delay between "checking" for wake-up command (in seconds)
const byte sleepTime = 30;
// Set max time our MCU will stay awake (in ms)
const int runTime = 300000;         // 5 minutes in milliseconds

// Set up SPI settings for XBee board
SPISettings sendSettings( 16000000, MSBFIRST, SPI_MODE0 );

void setup(){
    // Set the PT "load" as an output, to simulate powering a PT
    pinMode( PT_load, OUTPUT );
    digitalWrite( PT_load, LOW );   // PT is initially "off"

    // XBee slave-select output pin setup
    pinMode( ZB_ss, OUTPUT );
    digitalWrite( ZB_ss, HIGH );
    // Set the XBee attention pin as an input
    pinMode( ZB_attn, INPUT );
    // Set the ZigBee "wake" and "sleep" pin
    pinMode( ZB_on, OUTPUT );
    digitalWrite( ZB_on, HIGH );    // ZB is initially "on"
    
    // Begin communications to the Arduino "PT"
    Serial.begin( BAUDrate );

    // Start SPI interface
    SPI.begin();

    // Analog battery pin does not need setup
}

void loop(){
    sleep();                        // Start in the sleep state
    
    if( wakeUp )                    // If ZigBee gets a "wake" command
        wake();
}

// Do nothing for the majority of the sleep-cycle, and check for a "wake"
// command periodically (set by `sleepTime`)
void sleep(){
    char wakeCommand = 0x00;        // Commands from the GUI terminal
    
    // Put the ZigBee transmitter to sleep for ULP mode
    digitalWrite( ZB_on, LOW );
    
    sleepSeconds( sleepTime );      // Sleep for X-sec before cheking for wake
    
    // Wake the ZigBee transmitter
    digitalWrite( ZB_on, HIGH );
    
    // Check for a wake command after pre-set time
    updateFrameArray( "Checking for wake command..." );
    SPI.beginTransaction( sendSettings );
    digitalWrite( ZB_ss, LOW );
    wakeCommand = SPI.transfer( 0 );
    digitalWrite( ZB_ss, HIGH );
    SPI.endTransaction();
    
    // Wake the device if we received the command, leaving the ZigBee module on
    if( wakeCommand = wakeComm )
        wakeUp = true;
}

// Run commands for 5 minutes, and check for a "sleep" command between data
// transmissions.
void wake(){
    char sleepCommand = 0x00;       // Commands from the GUI terminal
    String transmitData,            // Data to transmit from the "PT"
           transmitBatt;            // Battery level to transmit to the GUI
    unsigned long timeToSleep;      // Keep track of when we go back to sleep

    // Sleep after right now + our predefined runtime
    timeToSleep = millis() + runTime;
    
    // Turn the PT "on"
    digitalWrite( PT_load, HIGH );

    // Transmit data to ZigBee
    digitalWrite( ZB_on, HIGH );
    
    // Perform actions while "awake"
    while( wakeUp ){
        // Wait for UART communication -- this dictates transmission freq.
        if( Serial.available() != 0 ){
            transmitData = Serial.readString();

            // Get battery level percentage for GUI
            transmitBatt = String( analogRead( battPin ) );

            // Send PT data and check for sleep command
            updateFrameArray( String( transmitBatt + ";" + transmitData ) );
            sleepCommand = transmitSPI();

            // If we get a "sleep" command, or our timer has expired, sleep
            if( sleepCommand = sleepComm || millis() >= timeToSleep )
                wakeUp = false;
        }

        // Check if our rut-time timer has expired
        if( millis() >= timeToSleep )
            wakeUp = false;
    }

    // End transmit connection to ZigBee
    digitalWrite( ZB_on, LOW);
    
    // Turn the PT "off"
    digitalWrite( PT_load, LOW );
}

// Transmit our datas via the SPI interface
byte transmitSPI(){
    // Open the SPI bus
    SPI.beginTransaction( sendSettings );
    
    digitalWrite( ZB_ss, LOW );
    SPI.transfer( &frameArray[0], frameArrayLength + 1 );
    byte ourReturn = SPI.transaction( 0 );
    digitalWrite( ZB_ss, HIGH );

    // Close the SPI bus
    SPI.endTransaction();

    return ourReturn;
}

// Populate our frame-data as per DIGI's XBee API
// https://www.digi.com/resources/documentation/Digidocs/90002002/Content/Reference/r_api_frame_format_900hp.htm
void updateFrameArray( String ourWord ){
    // Start frame indicator
    frameArray[0]  = startFrame;

    // Message size
    frameArray[1]  = messSize;
    // Length
    frameArray[2]  = getMessageSize( ourWord );

    // Frame type
    frameArray[3]  = frameType;
    // Frame ID
    frameArray[4]  = frameID;

    // MAC address of receiving device
    for( int i = 5; i < 13; i++ )
        frameArray[i] = recvMAC[i-5];

    // Frame options
    frameArray[13] = options;

    frameArrayLength = 14;

    // Load our word into the frame array
    while( ourWord[frameArrayLength - 14] != '\0' ){
        frameArray[frameArrayLength] = ourWord[frameArrayLength - 14];
        frameArrayLength++;
    }

    // Checksum
    frameArray[frameArrayLength] = getCheckSum( ourWord );
}

// Get the total number of bytes being sent, excluding the start delimeter, the
// length, and the checksum.
// https://www.digi.com/resources/documentation/Digidocs/90001942-13/concepts/c_api_frame_structure.htm?tocpath=XBee%20API%20mode%7C_____2
byte getMessageSize( String ourWord ){
    int count = 11;                     // Number of bytes including frameType,
                                        // frameID, MAC address, and options

    // Count the number of bytes in the message
    for( int i = 0; ourWord[i] != '\0'; i++ )
        count++;

    return count;
}

// Get the checksum of all bytes, with the exception of the start delimeter and
// the message size.
// https://www.digi.com/resources/documentation/Digidocs/90002002/Tasks/t_calculate_checksum.htm?TocPath=API%20Operation%7CAPI%20frame%20format%7C_____1
byte getCheckSum( String ourWord ){
    byte x = frameType + frameID;       // Add the first part of our frame data
    
    for( int i = 0; i < 8; i++ )
        x += recvMAC[i];                // Sum the bytes of the MAC address
        
    x += options;                       // Add our frame options

    for( int i = 0; ourWord[i] != '\0'; i++ )
        x += ourWord[i];                // Sum the bytes of the message

    // Subtract the lowest byte of our calculated value from 0xFF to find a
    // single byte for our checksum
    byte checksum = 0x0FF - (x & 0x0FF);
    
    return checksum;                    // Return our checksum
}

