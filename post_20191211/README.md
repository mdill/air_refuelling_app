# Collins Aerospace Air Refuelling Application

## Purpose

This is a common-source of all 4805/4806 Collins Aerospace refuelling project
resources.

## Directory Structure

The `bb` directory holds all datafiles and source code for the "black box" which
was used as a stand-in for the pressure-transducer (PT) developed by Collins
Aerospace.  This source code was originally intended to operate on an Arduino
with an SD card shield to house the immense data samples for fuel data.  As this
amount of fuel pressure data isn't necessary, and size and form-factor was
condensed for development simplification, the SD card shield was omitted and the
Arduino was changed from an UNO to a Pro Micro.

The `black_box_CAD` directory holds all AutoDesk Inventor files for the black
box files necessary to 3D print an enclosure for the Arduino Uno, SD card shield,
as well as various other parts.

The `black_box_schematic` directory holds the files required  to wire up the
Arduino stand-in for the PT.  This device works as a function generator,
providing UART serial data representing simulated pressure-data.

The `datasheets` directory holds all datasheets required for project development
and information.

The `msp` directory holds all source code for an MSP430G2 MCU.  This devices was
utilized for its ULP operation and small form-factor.  This source code was
eventually changed to Arduino code in favor of its rapid-development and the
smaller form-factor of the Arduino Nano over the MSP430G2553ET dev board.

The `msp_no_sleep` is a copy of the above directory, with the sleep/wake
functionality removed for testing purposes.

The `QT_GUI_interface` directory holds all C++ code for the Qt GUI.  This GUI
was designed to simultaneously display received pressure data as well as allow
the user to control the MCU outlined above.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/air_refuelling_app.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/air_refuelling_app/src/master/LICENSE.txt) file for
details.

