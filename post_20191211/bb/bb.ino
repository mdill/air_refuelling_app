/*
 * Michael Dill - 2019
 *
 * This code is written for ECE4806 and Collins Aerospace to simulate a pressure
 * transducer which provides UART communication between an in-tank device and a
 * ZigBee wireless transmitter.
 *
 * An Arduino will be used as a stand-in for the PT, and a pre-recorded
 * datastream will be used as a stand-in for an aircraft's fuel tank being
 * refilled with fuel.
 *
 * This Arduino will focus on data provided by an SD card, and "stream" the
 * pre-recorded data to UART terminals, which will later be interpreted by an
 * MCU and transmitted wirelessly.
 *
 * The Arduino will utilize an LCD screen to display the current values being
 * transmitted over UART for visual verification of displayed data.  It will
 * also switch green/red LEDs as a second visual indicator of the data-stream
 * either running or finished running, respectively, for device reset.
 *
 */

// Define our sampling frequency in Hz
#define freq        1
// Define our serial BAUD rate
#define BAUDrate    9600

// Define run/stop LEDs
#define greenLED    3
#define redLED      5

// To read data from the SD card
//#include <SD.h>
// Create a sample datastream array
String ourData[] = { "14.71881", "14.72013", "14.73595", "14.74683", "14.75492",
                     "14.76807", "14.77978", "14.78746", "14.79695", "14.81865",
                     "14.87199", "14.90994", "14.96207", "15.00361", "15.04803",
                     "15.13682", "15.20666", "15.36036", "15.36543", "15.38986",
                     "15.39938" };

// To print data to an LCD panel
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd( 0x3F, 16, 2 );

void setup(){
    // Set up run/stop LEDs
    pinMode( redLED, OUTPUT );
    pinMode( greenLED, OUTPUT );
    digitalWrite( redLED, LOW );
    digitalWrite( greenLED, HIGH );
    
    // Begin wired comms with MSP board
    Serial.begin( BAUDrate );

    // Begin LCD I2C interface
    lcd.init();
    lcd.backlight();
    
    /* We are not using the SD card shield for prototyping
    // Begin reading data from SD card
    SD.begin( 10 );                     // Need pin #10 for SD card bit-shifting
    File dataLog;                       // Store our data from the SD card
    dataLog = SD.open( "DATA.TXT", FILE_READ );

    // Prime our data, one character at a time
    char ourData[100];
    int currPos = 0;
    ourData[currPos] = dataLog.read();
    while( ourData[currPos] != '\n' && ourData[currPos] != '\r' ){
        currPos++;
        ourData[currPos] = dataLog.read();
    }
    ourData[currPos] = '\0';            // NULL the end of our "string"

    // Iterate through our SD card data
    while( dataLog.peek() != -1 ){
        Serial.write( ourData );        // Send our data to MSP board
        Serial.write( "\n" );
        lcdUpdate( ourData );           // Print our sent data to the LCD screen
        
        delay( 1 / freq * 1000 );       // Wait for our frequency, in ms
        
        // Re-prime our data
        currPos = 0;
        ourData[currPos] = dataLog.read();
        while( ourData[currPos] != '\n' && ourData[currPos] != '\r' ){
            currPos++;
            ourData[currPos] = dataLog.read();
        }
        ourData[currPos] = '\0';            // NULL the end of our "string"
    }
    
    // Close our data log to avoid corrupting SD card contents upon restart
    dataLog.close();
    */

    // Transmit our datastream array
    for( int i = 0; i < sizeof( ourData ) / sizeof( ourData[0] ); i++ ){
        Serial.print( ourData[i] );     // Send our data to MSP board
        Serial.print( "\n" );
        lcdUpdate( ourData[i] );        // Print our sent data to the LCD screen

        delay( 1/ freq * 1000 );        // Wait for our frequency, in ms
    }
    
    Serial.end();

    lcd.setCursor( 0, 0 );
    lcd.print( " Data transfer  " );
    lcd.setCursor( 0, 1 );
    lcd.print( " is complete... " );

    digitalWrite( greenLED, LOW );      // Turn off our green LED as we are done
    //digitalWrite( redLED, HIGH );
}

void loop(){
static int brightness = 0,
           fadeAmount = 5;

    // Fade the red LED to signal the end of our data transmission
    analogWrite( redLED, brightness );

    // Assure the LED doesn't go outside of our LOW/HIGH boundaries
    brightness += fadeAmount;
    if( brightness <= 0 || brightness >= 255 )
        fadeAmount = - fadeAmount;

    delay( 30 );
}

// Prints the current data being sent over UART to an LCD screen
void lcdUpdate( String ourData ){
    lcd.setCursor( 0, 0 );
    lcd.print( " Sending data:  " );
    lcd.setCursor( 0, 1 );
    lcd.print( "    " );
    lcd.print( ourData );
    lcd.print( "    " );
}

