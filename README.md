# Collins Aerospace Air Refuelling Application

## Purpose

The purpose of this project is to create and maintain a wireless standard for
digital communication between nodes.  Intended for aircraft refuelling
maintenance, one node will be placed in a fuel tank -- repeatable through a
mesh-network for multiple fuel tanks -- while the receiving node will be placed
in a terminal with a GUI.

The terminal(s) in the fuel tank(s) will communicate with a pressure transducer
(PT ) and will be solar powered -- this requires the system to store energy for
non-daylight hours.  All fuel-tank nodes to be ultra-low-powered (ULP) and
maintain power regulation practices.  For this purpose, an MCU is needed to both
translate UART serial data from the PT to the wireless transmitter as well as
switch the device on and off.

Due to its ULP operation, the DIGI XBee Zigbee transmitter was chosen.
Particularly, the S2C model was chosen for its SPI bus interface, as the PT has
already consumed the UART ports of the MCU.

A QT C++ GUI was designed to receive this transmitted data as well as control
the MCU's sleep/wake cycles.  This GUI accepts various user-defined conditional
variables (e.g. - aircraft stance on the TARMAC) as these would invariably
affect the PT's ability to read an accurate fuel pressure.

## Directory Structure

This is a common-source for all DART 4805/4806 Collins Aerospace refuelling
project resources.

These resources have been broken into two directories:
    * `pre_20191211`
    * `post_20191211`

These two directories separate all work into pre-expo work and post-expo work.
As all work done for the expo is saved as a milestone, and concurrent work is
forked as a tandem project, these directories serve to mirror start/stops of
project work.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/air_refuelling_app.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/air_refuelling_app/src/master/LICENSE.txt) file for
details.

