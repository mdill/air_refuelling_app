#include "air_refuelling_app.h"
#include "ui_air_refuelling_app.h"

#include <regex>
using std::regex;
using std::regex_search;
using std::smatch;

#include <QSerialPort>
#include <QSerialPortInfo>




// Configure this block prior to device installation
//===================================================================================================================
// Store our aircraft conditions
double currAltitude = 0.0,
       currPitch    = 0.0,
       currYaw      = 0.0,
       currRoll     = 0.0;

// This function needs to be changed per aircraft.  It will use the above variables
// to calculate an accurate fuel level based on the pressure exhibited by the fuel
// given the positional status of the aircraft.
double air_refuelling_app::getPressureFromEquation( double pressure ){
    double ourPressure = pressure ;      // Change this equation to compensate for alt, pitch, yaw, etc.

    return ourPressure;
}
//===================================================================================================================





// Track if our device is on or off
bool isAwake = true;

// Store our transmitted data
double fuelPressure = 0.0,
       battVoltage = 0.0;
const double maxBattLevel = 3.6;

// Parse our UART data
float uartPressure = 0.0,
      uartBattery  = 0.0;

// Store the COM port our XBee device is connected to
QString ourComPort;

air_refuelling_app::air_refuelling_app(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::air_refuelling_app)
{
    ui->setupUi(this);
    QMainWindow::showFullScreen();

    // Set up USB port drop-down menu
    updateComPort();
    QObject::connect( ui->selectUsbDropdown, SIGNAL( currentIndexChanged() ), this, SLOT( updateComPort() ) );

    // Set up exit/close button
    QObject::connect( ui->closeButton, SIGNAL( clicked() ), this, SLOT( exitSlot() ) );
    // Set up sleep/wake buttons
    QObject::connect( ui->sleepButton, SIGNAL( released() ), this, SLOT( sleepDevice() ) );
    QObject::connect( ui->wakeButton, SIGNAL( released() ), this, SLOT( wakeDevice() ) );

    // Initialize GUI display values
    ui->altitudeInput->setText( QString::number( currAltitude ) );
    ui->pitchInput->setText( QString::number( currPitch ) );
    ui->yawInput->setText( QString::number( currYaw ) );
    ui->rollInput->setText( QString::number( currRoll ) );

    // Update GUI display values when changed
    QObject::connect( ui->altitudeInput, SIGNAL( textEdited ), this, SLOT( updatePlaneStatus() ) );
    QObject::connect( ui->pitchInput, SIGNAL( textEdited ), this, SLOT( updatePlaneStatus() ) );
    QObject::connect( ui->yawInput, SIGNAL( textEdited ), this, SLOT( updatePlaneStatus() ) );
    QObject::connect( ui->rollInput, SIGNAL( textEdited ), this, SLOT( updatePlaneStatus() ) );

    // Initialize sleep/wake button states
    checkWakeSleepButtons();
}

air_refuelling_app::~air_refuelling_app()
{
    delete ui;
}

// Update the COM port address for XBee communication
void air_refuelling_app::updateComPort(){
    foreach( const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts() ){
        ui->selectUsbDropdown->addItem( serialPortInfo.portName() + " - " + serialPortInfo.manufacturer() );
    }

    ourComPort = ui->selectUsbDropdown->currentText();
}

// Close the GUI
void air_refuelling_app::exitSlot(){
    air_refuelling_app::close();
}

// Send the sleep command to the XBee unit
void air_refuelling_app::sleepDevice(){
    // Enable editing of variable input boxes
    ui->altitudeInput->setEnabled( true );
    ui->pitchInput->setEnabled( true );
    ui->yawInput->setEnabled( true );
    ui->rollInput->setEnabled( true );
    ui->selectUsbDropdown->setEnabled( true );

    isAwake = true;
    checkWakeSleepButtons();
}

// Send the wake command to the XBee unit and receive data
void air_refuelling_app::wakeDevice(){
    // Disable editing of variable input boxes
    ui->altitudeInput->setEnabled( false );
    ui->pitchInput->setEnabled( false );
    ui->yawInput->setEnabled( false );
    ui->rollInput->setEnabled( false );
    ui->selectUsbDropdown->setEnabled( false );

    isAwake = false;
    checkWakeSleepButtons();
/*
    // Read UART data from XBee board when it is awake
    while( isAwake ){
        updatePlaneStatus();        // Grab user-input values

        getUartData();              // Get data from USB

        // Change our fuel pressure GUI
        double currPressure = getPressureFromEquation( uartPressure ) / 20.0 * 100;
        ui->fuelProgressBar->setValue( currPressure );
        ui->fuelPercentLabel->setText( QString::number( currPressure ) );
        ui->fuelPsiLabel->setText( QString::number( uartPressure ) );

        // Change our battery level GUI
        double battVoltageLevel = uartBattery / 1023.0 * maxBattLevel;
        double battPercentLevel = uartBattery / 1023.0 * 100;
        ui->batteryProgressBar->setValue( battPercentLevel );
        ui->battPercentLabel->setText( QString::number( battPercentLevel ) );
        ui->battVoltageLabel->setText( QString::number( battVoltageLevel ) );

        isAwake = false;
    }
    */
}

// Change the status of sleep/wake buttons based on state
void air_refuelling_app::checkWakeSleepButtons(){
    ui->wakeButton->setEnabled( isAwake );
    ui->sleepButton->setEnabled( !isAwake );
}

// Refresh constants for pressure equation
void air_refuelling_app::updatePlaneStatus(){
    currAltitude = ui->altitudeInput->text().toDouble();
    currPitch = ui->pitchInput->text().toDouble();
    currYaw = ui->yawInput->text().toDouble();
    currRoll = ui->rollInput->text().toDouble();
}

// Use RegEx to parse data from XBee communications
void air_refuelling_app::getUartData(){
    // Use dummy data until USB interface works
    uartPressure = 0;
    uartBattery = 0;

    /*
    // Get our USB data
    QString receivedData;

    // Set our REGEX pattern to find (battStatus);(pressStatus)
    regex pattern( "(.+);(.+)" );
    smatch matches;
    regex_search( receivedData, matches, pattern );

    // Parse our REGEX pattern into variables
    uartPressure = matches[0];
    uartBattery = matches[1];
    */
}
