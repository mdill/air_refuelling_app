#ifndef AIR_REFUELLING_APP_H
#define AIR_REFUELLING_APP_H

#include <QMainWindow>

namespace Ui {
class air_refuelling_app;
}

class air_refuelling_app : public QMainWindow
{
    Q_OBJECT

public:
    explicit air_refuelling_app(QWidget *parent = nullptr);
    ~air_refuelling_app();

private:
    Ui::air_refuelling_app *ui;

private slots:
    void exitSlot();
    void sleepDevice();
    void wakeDevice();
    void checkWakeSleepButtons();
    void updatePlaneStatus();
    void updateComPort();

    double getPressureFromEquation( double pressure );
    void getUartData();
};

#endif // AIR_REFUELLING_APP_H
