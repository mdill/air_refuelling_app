EESchema Schematic File Version 4
LIBS:Function_Generator-cache
EELAYER 26 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "\"Black Box\" Function Generator"
Date "lun. 30 mars 2015"
Rev "2.0"
Comp "DART"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 5CA5083F
P 4100 4200
F 0 "R2" V 3893 4200 50  0000 C CNN
F 1 "R" V 3984 4200 50  0000 C CNN
F 2 "" V 4030 4200 50  0001 C CNN
F 3 "" H 4100 4200 50  0001 C CNN
	1    4100 4200
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5CA508FC
P 4100 3750
F 0 "R1" V 3893 3750 50  0000 C CNN
F 1 "R" V 3984 3750 50  0000 C CNN
F 2 "" V 4030 3750 50  0001 C CNN
F 3 "" H 4100 3750 50  0001 C CNN
	1    4100 3750
	0    1    1    0   
$EndComp
$Comp
L Device:LED redLED1
U 1 1 5CA50979
P 3600 3750
F 0 "redLED1" H 3591 3495 50  0000 C CNN
F 1 "LED" H 3591 3586 50  0000 C CNN
F 2 "" H 3600 3750 50  0001 C CNN
F 3 "" H 3600 3750 50  0001 C CNN
	1    3600 3750
	1    0    0    1   
$EndComp
$Comp
L Device:LED greenLED1
U 1 1 5CA509CE
P 3600 4200
F 0 "greenLED1" H 3591 3945 50  0000 C CNN
F 1 "LED" H 3591 4036 50  0000 C CNN
F 2 "" H 3600 4200 50  0001 C CNN
F 3 "" H 3600 4200 50  0001 C CNN
	1    3600 4200
	1    0    0    1   
$EndComp
Wire Wire Line
	3950 4200 3750 4200
Wire Wire Line
	3750 3750 3950 3750
Wire Wire Line
	3450 3750 3200 3750
Wire Wire Line
	3200 3750 3200 4200
Wire Wire Line
	5500 5250 5500 4950
Wire Wire Line
	3450 4200 3200 4200
Connection ~ 3200 4200
$Comp
L Switch:SW_Push Reset_Button1
U 1 1 5CA50E73
P 6600 3250
F 0 "Reset_Button1" H 6600 3535 50  0000 C CNN
F 1 "SW_Push" H 6600 3444 50  0000 C CNN
F 2 "" H 6600 3450 50  0001 C CNN
F 3 "" H 6600 3450 50  0001 C CNN
	1    6600 3250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3250 6100 3250
Wire Wire Line
	5700 4950 5700 5250
Wire Wire Line
	5700 5250 7000 5250
Wire Wire Line
	7000 5250 7000 3250
Wire Wire Line
	7000 3250 6800 3250
$Comp
L Device:Battery_Cell 9V1
U 1 1 5CA510F4
P 6750 2200
F 0 "9V1" V 6900 2250 50  0000 C CNN
F 1 "Battery_Cell" V 7000 2250 50  0000 C CNN
F 2 "" V 6750 2260 50  0001 C CNN
F 3 "" V 6750 2260 50  0001 C CNN
	1    6750 2200
	0    -1   -1   0   
$EndComp
$Comp
L Switch:SW_Push_SPDT On/Off_Switch1
U 1 1 5CA51368
P 6050 2200
F 0 "On/Off_Switch1" H 6050 2485 50  0000 C CNN
F 1 "SW_Push_SPDT" H 6050 2394 50  0000 C CNN
F 2 "" H 6050 2200 50  0001 C CNN
F 3 "" H 6050 2200 50  0001 C CNN
	1    6050 2200
	-1   0    0    -1  
$EndComp
Text GLabel 4650 3400 0    50   Output ~ 0
TX_wire
Text GLabel 4650 3250 0    50   Input ~ 0
RX_wire
Wire Wire Line
	5100 3350 4800 3350
$Comp
L Connector:Barrel_Jack Power1
U 1 1 5CACEEE2
P 5200 2400
F 0 "Power1" H 5255 2725 50  0000 C CNN
F 1 "Barrel_Jack" H 5255 2634 50  0000 C CNN
F 2 "" H 5250 2360 50  0001 C CNN
F 3 "" H 5250 2360 50  0001 C CNN
	1    5200 2400
	1    0    0    -1  
$EndComp
$Comp
L MCU_Module:Arduino_UNO_R3 MCU1
U 1 1 5CA50716
P 5600 3850
F 0 "MCU1" H 5900 5050 50  0000 C CNN
F 1 "Arduino_UNO_R3" H 5900 4950 50  0000 C CNN
F 2 "Module:Arduino_UNO_R3" H 5750 2800 50  0001 L CNN
F 3 "" H 5400 4900 50  0001 C CNN
	1    5600 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2200 6250 2200
Wire Wire Line
	5500 2500 5500 2850
Wire Wire Line
	5850 2300 5500 2300
Wire Wire Line
	6850 2200 7000 2200
Wire Wire Line
	7000 2200 7000 3250
Connection ~ 7000 3250
Wire Wire Line
	3200 4200 3200 5250
Wire Wire Line
	4850 3850 4850 4200
Wire Wire Line
	4850 3850 5100 3850
Wire Wire Line
	4650 3250 5100 3250
Wire Wire Line
	4800 3350 4800 3400
Wire Wire Line
	4800 3400 4650 3400
Wire Wire Line
	4250 3750 5100 3750
Wire Wire Line
	4250 4200 4850 4200
Wire Wire Line
	3200 5250 5500 5250
$EndSCHEMATC
