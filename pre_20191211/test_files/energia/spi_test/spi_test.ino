String testString = "Test string #";
unsigned long globalCount = 1;

#include <SPI.h>

const byte  startFrame  = 0x7E;
byte        messSize    = 0x00;
const byte  frameType   = 0x00;
const byte  frameID     = 0x01;
const byte  recvMAC[]   = { 0x00, 0x13, 0xA2, 0x00, 0x41, 0x94, 0xC9, 0xD6 };
const byte  options     = 0x00;
byte        checkSum    = 0x00;

#define     ZB_ss       8               // I/O pin P2.0
#define     ZB_attn     13              // I/O pin 2.5

int frameArrayLength;                   // Track how many bytes to send
byte frameArray[100];                   // Allocate enough memory for all bytes

// Set up our SPI settings for XBee board
SPISettings sendSettings( 16000000, MSBFIRST, SPI_MODE0 );

void setup(){
    // Set up UART serial comms
    Serial.begin( 9600 );

    // XBee slave select output pin
    pinMode( ZB_ss, OUTPUT );
    digitalWrite( ZB_ss, HIGH );

    // XBee attention input pin
    pinMode( ZB_attn, INPUT );

    // Start the SPI interface
    SPI.begin();
}

void loop(){
    //updateFrameArray( testString );
    
    updateFrameArray( testString + String( globalCount ) + "...\n" );
    Serial.println( testString + String( globalCount ) );
    globalCount++;

    transmitSPI();

    delay( 250 );                       // 4Hz update frequency
}

byte transmitSPI(){
    // Open the SPI bus
    SPI.beginTransaction( sendSettings );
    
    Serial.println( "Sent " + String(globalCount) );
    digitalWrite( ZB_ss, LOW );
    SPI.transfer( &frameArray[0], frameArrayLength + 1 );
    byte ourReturn = SPI.transaction( 0 );
    digitalWrite( ZB_ss, HIGH );

    // Close the SPI bus
    SPI.endTransaction();

    return ourReturn;
}

void updateFrameArray( String ourWord ){
    // Start frame indicator
    frameArray[0]  = startFrame;

    // Message size
    frameArray[1]  = messSize;
    // Length
    frameArray[2]  = getMessageSize( ourWord );

    // Frame type
    frameArray[3]  = frameType;
    // Frame ID
    frameArray[4]  = frameID;

    // MAC address of receiving device
    for( int i = 5; i < 13; i++ )
        frameArray[i] = recvMAC[i-5];

    // Frame options
    frameArray[13] = options;

    frameArrayLength = 14;

    // Load our word into the frame array
    while( ourWord[frameArrayLength - 14] != '\0' ){
        frameArray[frameArrayLength] = ourWord[frameArrayLength - 14];
        frameArrayLength++;
    }

    // Checksum
    frameArray[frameArrayLength] = getCheckSum( ourWord );
}

// Get the total number of bytes being sent, excluding the start delimeter, the
// length, and the checksum.
// https://www.digi.com/resources/documentation/Digidocs/90001942-13/concepts/c_api_frame_structure.htm?tocpath=XBee%20API%20mode%7C_____2
byte getMessageSize( String ourWord ){
    int count = 11;                     // Number of bytes including frameType,
                                        // frameID, MAC address, and options

    // Count the number of bytes in the message
    for( int i = 0; ourWord[i] != '\0'; i++ )
        count++;

    return count;
}

// Get the checksum of all bytes, with the exception of the start delimeter and
// the message size.
// https://www.digi.com/resources/documentation/Digidocs/90002002/Tasks/t_calculate_checksum.htm?TocPath=API%20Operation%7CAPI%20frame%20format%7C_____1
byte getCheckSum( String ourWord ){
    byte x = frameType + frameID;       // Add the first part of our frame data
    
    for( int i = 0; i < 8; i++ )
        x += recvMAC[i];                // Sum the bytes of the MAC address
        
    x += options;                       // Add our frame options

    for( int i = 0; ourWord[i] != '\0'; i++ )
        x += ourWord[i];                // Sum the bytes of the message

    // Subtract the lowest byte of our calculated value from 0xFF to find a
    // single byte for our checksum
    byte checksum = 0x0FF - (x & 0x0FF);
    
    return checksum;                    // Return our checksum
}

