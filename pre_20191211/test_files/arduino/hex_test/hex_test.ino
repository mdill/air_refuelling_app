#include <SPI.h>

const byte  startFrame  = 0x7E;
byte        messSize    = 0x00;
const byte  frameType   = 0x00;
const byte  frameID     = 0x01;
const byte  recvMAC[]   = { 0x00, 0x13, 0xA2, 0x00, 0x41, 0x94, 0xC9, 0xD6 };
const byte  options     = 0x00;
byte        checkSum    = 0x00;

#define     ZB_ss       8               // I/O pin P2.0
#define     ZB_attn     13              // I/O pin 2.5

int frameArrayLength;                   // Track how many bytes to send
byte frameArray[100];                   // Allocate enough memory for all bytes

void setup(){
    // Set up UART serial comms
    Serial.begin( 9600 );

    // XBee slave select output pin
    pinMode( ZB_ss, OUTPUT );
    digitalWrite( ZB_ss, HIGH );

    // XBee attention input pin
    pinMode( ZB_attn, INPUT );

    // Set up SPI comms
    SPI.begin();
    SPI.setBitOrder( MSBFIRST );        // Most significant bit first
    SPI.setDataMode( SPI_MODE0 );       // Leading edge samples at rising edge
    //SPI.setClockDivider( 16 );          // 1MHz from a 16MHz MSP430g2 clock
}

void loop(){
    updateFrameArray( "test string" );
    
    /*
    //transmitSPI( "d" );
    digitalWrite( ZB_ss, LOW );
    SPI.transfer( &frameArray[0], frameArrayLength );
    digitalWrite( ZB_ss, HIGH );

    // Send a newline char over UART
    Serial.write( "Sent #" + count );
    count++;
    */

    // Send our array bytes through the console
    Serial.print( "startFrame:\t" );
        Serial.println( frameArray[0], HEX );
    Serial.print( "messSize:\t" );
        Serial.println( frameArray[1], HEX );
    Serial.print( "length:\t\t" );
        Serial.println( frameArray[2], HEX );
    Serial.print( "frameType:\t" );
        Serial.println( frameArray[3], HEX );
    Serial.print( "frameID:\t" );
        Serial.println( frameArray[4], HEX );
    Serial.print( "recvMAC:\t" );
        Serial.print( frameArray[5], HEX );
        Serial.print( "\n\t\t" );
        Serial.print( frameArray[6], HEX );
        Serial.print( "\n\t\t" );
        Serial.print( frameArray[7], HEX );
        Serial.print( "\n\t\t" );
        Serial.print( frameArray[8], HEX );
        Serial.print( "\n\t\t" );
        Serial.print( frameArray[9], HEX );
        Serial.print( "\n\t\t" );
        Serial.print( frameArray[10], HEX );
        Serial.print( "\n\t\t" );
        Serial.print( frameArray[11], HEX );
        Serial.print( "\n\t\t" );
        Serial.println( frameArray[12], HEX );
    Serial.print( "options:\t" );
        Serial.println( frameArray[13], HEX );
    Serial.print( "word:\t\t" );
        Serial.print( frameArray[14], HEX );
        for( int i = 15; i < frameArrayLength; i++ ){
            Serial.print( "\n\t\t" );
            Serial.print( frameArray[i], HEX );
        }
        Serial.print( '\n' );
    Serial.print( "checksum:\t" );
        Serial.println( frameArray[frameArrayLength], HEX );

    // Get array-end delimeter
    Serial.println( frameArray[100] );
        
    delay( 1000 );
    Serial.print( '\n' );
}

/*
void transmitSPI( String ourWord ){
    byte messageSize = getMessageSize( ourWord );
    byte checkSum = getCheckSum( ourWord );

    frameArray[2]  = messageSize;

    int byteSize = messageSize + 0;
    
    digitalWrite( ZB_ss, LOW );
    SPI.transfer( &frameArray, byteSize );
    digitalWrite( ZB_ss, HIGH );
}
*/

void updateFrameArray( String ourWord ){
    // Start frame indicator
    frameArray[0]  = startFrame;

    // Message size
    frameArray[1]  = messSize;
    // Length
    frameArray[2]  = getMessageSize( ourWord );

    // Frame type
    frameArray[3]  = frameType;
    // Frame ID
    frameArray[4]  = frameID;

    // MAC address of receiving device
    for( int i = 5; i < 13; i++ )
        frameArray[i] = recvMAC[i-5];

    // Frame options
    frameArray[13] = options;

    frameArrayLength = 14;

    // Load our word into the frame array
    while( ourWord[frameArrayLength - 14] != '\0' ){
        frameArray[frameArrayLength] = ourWord[frameArrayLength - 14];
        frameArrayLength++;
    }

    // Checksum
    frameArray[frameArrayLength] = getCheckSum( ourWord );
}

// Get the total number of bytes being sent, excluding the start delimeter, the
// length, and the checksum.
// https://www.digi.com/resources/documentation/Digidocs/90001942-13/concepts/c_api_frame_structure.htm?tocpath=XBee%20API%20mode%7C_____2
byte getMessageSize( String ourWord ){
    int count = 11;                     // Number of bytes including frameType,
                                        // frameID, MAC address, and options

    // Count the number of bytes in the message
    int i = 0;
    while( ourWord[i] != '\0' ){
        count++;
        i++;
    }

    return count;
}

// Get the checksum of all bytes, with the exception of the start delimeter and
// the message size.
// https://www.digi.com/resources/documentation/Digidocs/90002002/Tasks/t_calculate_checksum.htm?TocPath=API%20Operation%7CAPI%20frame%20format%7C_____1
char getCheckSum( String ourWord ){
    char x = frameType + frameID;       // Add the first part of our frame data
    
    for( int i = 0; i < 8; i++ )
        x += recvMAC[i];                // Sum the bytes of the MAC address
        
    x += options;                       // Add our frame options

    int i = 0;
    while( ourWord[i] != '\0' ){
        x += ourWord[i];                // Sum the bytes of the message
        i++;
    }

    // Subtract the lowest byte of our calculated value from 0xFF to find a
    // single byte for our checksum
    char checksum = 0xFF - (x & 0xFF);
    
    return checksum;                    // Return our checksum
}
