# Collins Aerospace Air Refuelling Application

## Purpose

This is a common-source of all 4805/4806 Collins Aerospace refuelling project
resources.

The `black_box` directories hold the files required for the pressure-transducer
replacement function generator.  This is built from an Arduino Uno, encased in
a literal black box.

The `UI` directories hold the files required for the receiving apps, transmitted
by the wireless project devices.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/air_refuelling_app.git

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/air_refuelling_app/src/master/LICENSE.txt) file for
details.

