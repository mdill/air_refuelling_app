/*
 * 20190403 - Michael Dill
 * 
 * This sketch is designed to replace the pressure transmitter, designed by
 * Collins Aerospace, with a rudimentary function generator of 3.3V and 35mW.
 *
 * This Arduino sketch immediately begins simulating roughly 30 minutes of
 * refuelling data which was provided by CA.  During this simulation time, a
 * green LED is illuminated.  Once the simulation is completely exhausted, a
 * red LED illuminates, and will stay illuminated until a pushbutton is pressed
 * to reset the Arduino and restart the simulation.
 *
 * ONE INPUT:
 *      Reset button
 * 
 * THREE OUTPUTS:
 *      Red LED
 *      Green LED
 *      Serial (TX/RX) wire output at 115200 BAUD
 *
 */

// Include SD Shield support
#include <SD.h>
#include <SPI.h>

// Import dataStream.h for simulation data stream array
#include "dataStream.h"

// Set loop condition
bool runLoop = 1;

// Set pin numbers for each LED
byte gLED = 6,
     rLED = 5;

// Set up our time tracking
unsigned long currentTime, oldTime;

// Set up datastream array
unsigned int dataLocation = 0;
const unsigned int maxData = 62204;

void setup(){
    // Open the dataStream text file for data points
    dataStream = SD.open( 'dts.txt' ); // Filename MUST be lesst than 3 chars

    // Set the LEDs as output indicators
    pinMode( gLED, OUTPUT );
    pinMode( rLED, OUTPUT );

    // Turn our LEDs on/off
    digitalWrite( gLED, HIGH );
    digitalWrite( rLED, LOW );

    // Set up the serial data system
    Serial.begin( 115200 );

    // Start our system clock
    currentTime = millis();
    oldTime = currentTime;
}

// Send our data over the serial connection
void sendData( float x ){

    Serial.println( x );
    dataLocation++;

    if( dataLocation >= maxData )
        killProgram();
}

// Stop our program when the datastream is finished
void killProgram(){
    digitalWrite( gLED, LOW );
    digitalWrite( rLED, HIGH );

    runLoop = 0;
}

void loop(){
    // Run program until we've reach the end of the datastream
    while( runLoop ){
        // Get the current runtime
        currentTime = millis();

        // If 25ms has passed since the last serial transmission, send the next 
        if( currentTime >= oldTime + 250 ){
            oldTime = currentTime;

            // Get and send our data over UART
            data = dataStream.readStringUntil( '\n' );
            sendData( data );
        }
    }
}

